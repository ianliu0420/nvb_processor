

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class Tools {

    public static float getDistance(int p1x, int p1y, int p2x, int p2y) {
        return (float) Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y));
    }

    public static float getDistance(float p1x, float p1y, float p2x, float p2y) {
        return (float) Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y));
    }

    public static float getDistance(float[] allpoints, int a, int b) {
        return (float) Math
                        .sqrt((allpoints[a * 2] - allpoints[b * 2]) * (allpoints[a * 2] - allpoints[b * 2])
                                        + (allpoints[a * 2 + 1] - allpoints[b * 2 + 1])
                                        * (allpoints[a * 2 + 1] - allpoints[b * 2 + 1]));
    }

    public int emoParser(String emo) {

        if ("neutral".equals(emo)) {
            return 0;
        } else if ("anger".equals(emo)) {
            return 1;
        } else if ("contempt".equals(emo)) {
            return 2;
        } else if ("disgust".equals(emo)) {
            return 3;
        } else if ("fear".equals(emo)) {
            return 4;
        } else if ("happy".equals(emo)) {
            return 5;
        } else if ("sadness".equals(emo)) {
            return 6;
        } else if ("surprise".equals(emo)) {
            return 7;
        }

        return -1;
    }



    public static boolean mergeTextFiles(List<String> inputFiles, String outputFile) {

        try {
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            
            for (String inputFile : inputFiles) {

                BufferedReader in = new BufferedReader(new FileReader(inputFile));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                    out.write(line);
                    out.newLine();
                    line = in.readLine();
                }
                in.close();
            }

            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    
    
    public static boolean mergeTextFilesWithLine(List<String> inputFiles, String outputFile, int numberPerFile) {

        try {
            BufferedWriter out=new BufferedWriter(new FileWriter(outputFile));
            
            // for (String inputFile : inputFiles) {
           for(int i =0; i<inputFiles.size(); i++) {
        	    int preNumber = numberPerFile * i;
        	    String inputFile = inputFiles.get(i);
                BufferedReader in = new BufferedReader(new FileReader(inputFile));
                String line = in.readLine();
                while (line != null && !"".equals(line.trim())) {
                	String[] values= line.split(":");
                	int lineNo = Integer.parseInt(values[0])+preNumber;
                	line = lineNo+":"+values[1];
                    out.write(line);
                    out.newLine();
                    line = in.readLine();
                }
                in.close();
            }

            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


    public static void main(String[] args) {
        
        
    }
    
    
    

}
