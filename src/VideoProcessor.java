import java.util.Date;

public class VideoProcessor {

	public static void main(String[] args) throws Exception{
		
		VideoProcessor vp  = new VideoProcessor();
		vp.process("video_1458343715068");
		
	}
	
	public void process(String fileName) throws Exception{
		processDoctor(fileName);
		processPatient(fileName);
	}
	
	
	public void processDoctor(String fileName) throws Exception{
		
		String audioFileBaseDirStr = "C:/videos/"+fileName+"/";
		String doctorFile = fileName + "_doctor";
        String patientFile = fileName + "_patient";
        int FPS = 25;
		
        System.out.println("before audio proc: " + new Date());
        SoundProcessorNoSync processorDoctor =
                        new SoundProcessorNoSync(audioFileBaseDirStr, doctorFile, audioFileBaseDirStr, doctorFile);
        processorDoctor.run();

        SoundProcessorNoSync processorPatient =
                        new SoundProcessorNoSync(audioFileBaseDirStr, patientFile, audioFileBaseDirStr, patientFile);
        processorPatient.run();
        System.out.println("after audio proc: " + new Date());
        
        
		ParserAllpoints ap = new ParserAllpoints();
        ap.getFaceSizeFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                        + "_leanings.txt");
        ap.getNosePointFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                        + "_nosepoint.txt");
        ap.getFaceMovement(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile + "_movement.txt");
        
    //  -------------------------- nodding -----------------------------------------------
        ParserNodding tn = new ParserNodding();
       // detect the nodding
       tn.manipXYNodding(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                       + "_nodding.txt", FPS);
       // detect the shaking
        tn.manipXYShaking(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                + "_shaking.txt", 25.0f);
        
        tn.deleteRepeat(audioFileBaseDirStr + doctorFile+ "_shaking.txt", 
                audioFileBaseDirStr + doctorFile + "_nodding.txt", 
                audioFileBaseDirStr + doctorFile+ "_shakingnew.txt", 
                audioFileBaseDirStr + doctorFile + "_noddingnew.txt");
        

        // -------------------------- tilting -----------------------------------------------
        ParserTilting td = new ParserTilting();
        int totalLineNo = td.getFileLineNos(audioFileBaseDirStr + doctorFile + "_allpoints.txt");
        td.generateTiltingAngleFile(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr
                        + doctorFile + "_allpoints_angle", totalLineNo, FPS);
        td.generateDurationFile(audioFileBaseDirStr + doctorFile + "_allpoints_angle_posnag", audioFileBaseDirStr
                        + doctorFile + "_allpoints_turns");

       // analyze the smile intensity
       ParserSmile st = new ParserSmile();
       st.analysisSmile(audioFileBaseDirStr + doctorFile + "_smile.txt", audioFileBaseDirStr + doctorFile
                       + "_smileana.txt", 25); 
       
       System.out.println("finish processing: " + new Date());
		
	}
	
public void processPatient(String fileName) throws Exception{
		
		String audioFileBaseDirStr = "C:/videos/"+fileName+"/";
		String doctorFile = fileName + "_patient";
        int FPS = 25;
		
		ParserAllpoints ap = new ParserAllpoints();
        ap.getFaceSizeFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                        + "_leanings.txt");
        ap.getNosePointFromAll(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile
                        + "_nosepoint.txt");
        ap.getFaceMovement(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr + doctorFile + "_movement.txt");
        
    //  -------------------------- nodding -----------------------------------------------
        ParserNodding tn = new ParserNodding();
       // detect the nodding
       tn.manipXYNodding(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                       + "_nodding.txt", FPS);
       // detect the shaking
        tn.manipXYShaking(audioFileBaseDirStr + doctorFile + "_nosepoint.txt", audioFileBaseDirStr + doctorFile
                + "_shaking.txt", 25.0f);
        
        tn.deleteRepeat(audioFileBaseDirStr + doctorFile+ "_shaking.txt", 
                audioFileBaseDirStr + doctorFile + "_nodding.txt", 
                audioFileBaseDirStr + doctorFile+ "_shakingnew.txt", 
                audioFileBaseDirStr + doctorFile + "_noddingnew.txt");
        

        // -------------------------- tilting -----------------------------------------------
        ParserTilting td = new ParserTilting();
        int totalLineNo = td.getFileLineNos(audioFileBaseDirStr + doctorFile + "_allpoints.txt");
        td.generateTiltingAngleFile(audioFileBaseDirStr + doctorFile + "_allpoints.txt", audioFileBaseDirStr
                        + doctorFile + "_allpoints_angle", totalLineNo, FPS);
        td.generateDurationFile(audioFileBaseDirStr + doctorFile + "_allpoints_angle_posnag", audioFileBaseDirStr
                        + doctorFile + "_allpoints_turns");

       // analyze the smile intensity
       ParserSmile st = new ParserSmile();
       st.analysisSmile(audioFileBaseDirStr + doctorFile + "_smile.txt", audioFileBaseDirStr + doctorFile
                       + "_smileana.txt", 25); 

       System.out.println("finish processing: " + new Date());
		
		
	}
	
}
